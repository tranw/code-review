import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from '../components/Login';
import Nav from '../components/Nav';
import Home from '../components/Home';
import CodeView from '../components/CodeView';
import Register from '../components/Register';
import Search from '../components/Search';
import Admin from '../components/Admin';
import { AuthRoute } from '../utils/AuthService';

export default () => (
    <BrowserRouter>
        <div>
            <Nav />
            <Switch>
                <Route exact path="/login" render={props => <Login {...props} />} />
                <Route exact path="/register" component={Register} />
                <AuthRoute exact path="/search" component={Search} />
                <AuthRoute exact path="/code/:id" component={CodeView} />
                <AuthRoute exact path="/" component={Home} />
                <AuthRoute exact path="/admin" component={Admin} />
            </Switch>
        </div>
    </BrowserRouter>
);
