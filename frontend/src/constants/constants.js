let static_folder = '/static/'

export let url = (process.env.NODE_ENV === "development") ? 
                        { baseURL: 'http://localhost:8000/api/' } : { baseURL: '/api/' };

export const languageOptions = [
	{ 
		text: 'Python', 
		value: 'Python', 
		image: { 
			avatar: true, 
			src: static_folder + 'lang_icons/python.ico' 
		} 
	},            
	{
        text: 'Java',
        value: 'Java',
        image: { avatar: true, src: static_folder + 'lang_icons/java.svg' }
    },            
	{
        text: 'C',
        value: 'C',
        image: { avatar: true, src: static_folder + 'lang_icons/c.jpg' }
    },
    {
        text: 'C#',
        value: 'C#',
        image: { avatar: true, src: static_folder + 'lang_icons/seesharp.png' }
    },
    {
        text: 'HTML/CSS',
        value: 'HTML/CSS',
        image: { avatar: true, src: static_folder + 'lang_icons/html.png' }
    },
    {
        text: 'JavaScript',
        value: 'JavaScript',
        image: { avatar: true, src: static_folder + 'lang_icons/javascript.svg' }
    },
    {
        text: 'Rust',
        value: 'Rust',
        image: { avatar: true, src: static_folder + 'lang_icons/rust.png' }
    }
];

export const getPicture = (language) => {
    let ret = '';

    if (typeof language === "undefined") {
        return ret;
    }

    languageOptions.forEach((el) => {
        if (el.text.toLowerCase() === language.toLowerCase()) {
            ret = el.image.src;
            return;
        }
    })

    return ret;
};

