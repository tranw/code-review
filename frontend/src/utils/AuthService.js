import React from 'react';
import axios from 'axios';
import { Route, Redirect } from 'react-router-dom';
import { url } from '../constants/constants';

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

export const instance = axios.create(url);

export const loggedIn = () => {
   return window.localStorage.getItem('token') !== null;
};

export const getTokenHeader = () => {
    return "Token " + getToken();
};

export const getTokenHeaderDict = () => {
    return { 'Authorization' : getTokenHeader() }
};

export const getToken = () => {
    return window.localStorage.getItem('token');
};

export const login = (username, password, props, onFailure) => {
    if (loggedIn()) {
        return true;
    }

    instance.post('/accounts/get_auth_token/', { username, password } )
        .then(response => {
            window.localStorage.setItem('token', response.data.token);
            props.history.replace('/');
        })
        .catch((error) => {
            onFailure(error);
        });
};

export const AuthRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        loggedIn() ? (
            <Component {...props} />
        ) : (
            <Redirect to={{pathname: '/login'}}/>
        )
    )}/>
);
