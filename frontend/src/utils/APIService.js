import { getTokenHeader, instance } from './AuthService';

export const getUserFiles = () => instance.get('/review/', {
  headers: { "Content-Type": "application/json", "Authorization": getTokenHeader() }
});

export const reUploadFile = (id, data, token) => instance.put('/review/' + id + '/', data, {
  headers: { "Content-Type": "application/json", "Authorization": token }
});

export const getCodeFromId = (id) => instance.get('/review/' + id + '/', {
  headers: { "Content-Type": "application/json", "Authorization": getTokenHeader()
}});
