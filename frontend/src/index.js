import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './utils/registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
const App = () => (
    <Routes />
);

ReactDOM.render((
   <BrowserRouter>
        <App/>
   </BrowserRouter>
), document.getElementById('root'));
registerServiceWorker();