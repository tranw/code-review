import { loggedIn, getTokenHeaderDict, instance } from '../utils/AuthService';
import { Link } from 'react-router-dom';
import React, { Component } from 'react'
import {
    Icon,
    Menu
} from 'semantic-ui-react';
import '../style.css';

export default class Nav extends Component {
    state = { activeItem: 'home', isAdmin: false };
    handleItemClick = (e, { name }) => this.setState({ activeItem: name });
    logout = () => {
        window.localStorage.removeItem('token');
    };

    componentWillMount = () => {
        instance.get('/accounts/get_user_info/', {
            headers: getTokenHeaderDict()
        })
            .then((z) => {
                if (z.data.is_superuser) {
                    this.setState({ isAdmin: true })
                }
            })
    }

    render() {
        const { activeItem } = this.state;
        const adminTile = (
            <Menu.Item color={'red'} as={Link} to='/admin' name='admin' active={activeItem === 'admin'} onClick={this.handleItemClick}>
                <Icon name='file' />
                Admin
            </Menu.Item>
        )

        return (
            loggedIn() ?
                <Menu style={{ 'height': '100%' }} color={'teal'} icon='labeled' inverted fixed={'left'} vertical>
                    <Menu.Item as={Link} to='/' name='profile' active={activeItem === 'profile'} onClick={this.handleItemClick}>
                        <Icon name='user' />
                        Profile
                    </Menu.Item>
                    <Menu.Item as={Link} to='/search' name='search' active={activeItem === 'search'} onClick={this.handleItemClick}>
                        <Icon name='search' />
                        Search
                    </Menu.Item>
                    {this.state.isAdmin ? adminTile : <div> </div>}
                    <Menu.Item id='logout' as={Link} to='/' name='logout' onClick={this.logout}>
                        <Icon name='chevron left' />
                        Logout
                    </Menu.Item>
                </Menu>
                : <div></div>
        )
    }
}
