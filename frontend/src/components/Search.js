import '../style.css';
import ReactDOM from 'react-dom';
import { getPicture } from '../constants/constants';
import React, { Component } from 'react';
import { instance, getTokenHeader } from '../utils/AuthService.js';
import { Button, Input, Segment, Card, Image, Label, Icon } from 'semantic-ui-react';
import Highlight from 'react-highlight';

export default class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchRequest: "",
            results: [],
        }

        this.onChange = this.onChange.bind(this);
        this.submitRequest = this.submitRequest.bind(this);
    }

    onChange(e) {
        this.setState({
            searchRequest: e.target.value
        })
    }

    submitRequest() {
        instance.get('/review/search/' + this.state.searchRequest, {
            headers: {
                'Authorization': getTokenHeader()
            }
        })
        .then((z) => {
            let key = 0;
            const results = z.data.map((file) => {
                key++;
                return <SearchResult
                    history={this.props.history}
                    key={key}
                    id={file.id}
                    title={file.title}
                    language={file.language}
                    code={file.code}
                    comments={file.comments}
                />
            });

            this.setState({
                results: results,
                loading: false
            })

            ReactDOM.render(
                <div>
                    {this.state.results}
                </div>,
                document.getElementById('search-mount')
            );

            console.log(z);
        })
        .catch((z) => {

        });
    }

    render() {
        return (
            <div style={{width: '100%', fontSize: '24px'}}>
                <div id='search-area' className='ui action input' style={{margin:'0px', backgroundColor: 'black', color: 'white'}}>
                    <Input
                        placeholder='Search...'
                        value={this.state.searchRequest}
                        onChange={this.onChange}
                        action={
                            {
                                onClick: this.submitRequest,
                                content: "Search",
                                labelPosition: "right",
                                icon: "search",
                                color: "teal"
                            }
                        }
                        style={{
                            width: '100%',
                            height: '100%',
                            backgroundColor: 'black',
                            margin: '0px'
                        }}
                    >
                    </Input>
                </div>
                <div id='search-mount' />
            </div>
        );
    }
}

class SearchResult extends Component {
    render() {
        return (
            <Card fluid={true} onClick={() => {this.props.history.push('/code/' + this.props.id)}} style={{margin: '0px'}}>
                <Card.Content>
                    <Image floated='left' size='mini' src={getPicture(this.props.language)} />
                    <Card.Header>
                        {this.props.title}
                        <Label style={{marginLeft: '12px', float: 'right'}}>
                            Comments: {this.props.comments.length}
                        </Label>
                        <Label style={{marginLeft: '12px', float: 'right'}}>
                            Lines: {getLines(this.props.code)}
                        </Label>
                    </Card.Header>
                    <Card.Meta>
                        {this.props.language}
                    </Card.Meta>
                    <Card.Description>
                        <code>{this.props.code.substring(0, 200)}</code>
                    </Card.Description>
                </Card.Content>
            </Card>
        );
    }
}

const getLines = (code) => {
    return code.split('\n').length;
} 
