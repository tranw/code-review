import '../style.css';
import CodeBlock, { Line } from './CodeBlock';
import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { getTokenHeader, instance } from '../utils/AuthService';
import { reUploadFile, getCodeFromId } from '../utils/APIService';
import Highlight from 'react-highlight';
import { Grid, Form, TextArea, Button, Header, Segment, Icon, Modal, Image, Input } from 'semantic-ui-react';


export default class CodeView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            language: "",
            code: "",
            title: "",
            modalOpen: false,
            reupload: "",
            commentStartLine: -1,
            commentEndLine: -1,
            commenting: false,
            comment: "",
            owner: true
        };
        this.handleReUpload = this.handleReUpload.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleReUpload(e) {
        const token = getTokenHeader();
        const data = {
            "language": this.state.language,
            "code": this.state.reupload,
            "title": this.state.title,
        };
        reUploadFile(this.props.match.param.id, data, token)

    }

    handleChange(e) {
        this.setState({
            reupload: e.target.value
        })
    }

    componentDidMount() {
        window.addEventListener('mousedown', this.updateStartLine);
        window.addEventListener('mouseup', this.updateEndLine);
    }

    componentWillUnmount() {
        window.removeEventListener('mousedown', this.updateStartLine);
        window.removeEventListener('mouseup', this.updateEndLine);
    }

    updateEndLine = (e) => {
        if (e.target.className === 'code-line') {
            this.setState({
                commentEndLine: e.target.id,
                commenting: true
            })
        }
    }

    updateStartLine = (e) => {
        if (e.target.className === 'code-line') {
            this.setState({
                commentStartLine: e.target.id
            })
        }
    }

    getLineCount = (code) => {
      var i = code.length;
      var count = 1;
      while(i--) {
        if (code.charAt(i) == '\n'){
          count++;
        }
      }
      return count;
    }

    componentWillMount() {

      getCodeFromId(this.props.match.params.id).then((z) => {
          this.setState({
              title: z.data.title,
              language: z.data.language,
              code: z.data.code
          })
          var lineCount = this.getLineCount(z.data.code);

          // setting right side comment section in view
          var comment_array = [];
          for (var x = 0; x < lineCount; x++) {
              comment_array[x] = { line_start: x, line_end: -1, comment: '', hasComment: false }
          }
            for (var x in z.data.comments) {
                var comment_line = z.data.comments[x].line_start - 1;
                comment_array[comment_line].hasComment = true;
                comment_array[comment_line].comment = z.data.comments[x].text;
                comment_array[comment_line].line_start = comment_line;
                comment_array[comment_line].line_end = z.data.comments[x].line_end;
            }

            //Render the newly received code lines
            ReactDOM.render(
                <CodeBlock lineCount={lineCount} code={z.data.code} language={z.data.language} title={z.data.title} comments={comment_array} />,
                document.getElementById('code-root')
            );
        })
    }

    setText = () => {
        let key = 0;
        return this.state.code.split('\n').map((e) => {
            key++;
            if (key >= this.state.commentStartLine &&
                    key <= this.state.commentEndLine) {
                return (
                    <div>
                        <code>
                            {e}
                        </code>
                    </div>
                );
            }
        })
    }

    submitComment = () => {
        let data = {
            id: this.props.match.params.id,
            line_start: this.state.commentStartLine,
            line_end: this.state.commentEndLine,
            text: this.state.comment
        }
        instance.post('/review/comment/', data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": getTokenHeader()
            }
        })
        .then((response) => {
            this.setState({
                commenting: false,
                comment: ""
            })
        })
    }

    // Displays code, allows user to re-submit (incomplete)
    render() {
        return (
            <div>
                <Modal open={this.state.commenting} size={'fullscreen'} style={{height: '100%'}}>
                    <Modal.Header>
                        Commenting..
                        <p>Lines {this.state.commentStartLine} - {this.state.commentEndLine} </p>
                    </Modal.Header>
                    <Modal.Content>
                        <Grid columns={2} centered divided>
                            <Grid.Row>
                                <Grid.Column>
                                    <Form>
                                        <TextArea
                                            onChange={(e) => {
                                                this.setState({
                                                    comment: e.target.value
                                                })
                                            }}
                                            value={this.state.comment}
                                        />
                                    </Form>
                                </Grid.Column>
                                <Grid.Column>
                                    <Highlight>
                                        {
                                            this.setText()
                                        }
                                    </Highlight>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                        <Modal.Actions style={{marginTop: '12px'}}>
                            <Button color='red' onClick={() => {this.setState({commenting: false})}}>
                                <Icon name='cancel' /> Cancel
                            </Button>
                            <Button color='green' onClick={this.submitComment}>
                                <Icon name='checkmark' /> Submit
                            </Button>
                        </Modal.Actions>
                    </Modal.Content>
                </Modal>
                <div style={{ height: '100%', width: '100%' }}>
                    <div>
                        <Grid id='codeview-block-outer' centered style={{ height: '100%' }} verticalAlign='middle'>
                            <Grid.Column style={{}}>
                                <Segment id='codeview-block-inner' stacked style={{ width: '100%', height: '100%' }}>
                                    <Header id='title-code' as='h1'>
                                        {this.state.title}<br />{"Language: " + this.state.language}
                                    </Header>
                                    <div id='code-root'>
                                    </div>
                                    {/* THIS IS FOR RE-UPLOADING CODE {
                                        (this.state.owner) ? (
                                            <Form size="large">
                                                <Form.Field
                                                    id='form-textarea-control-opinion'
                                                    name='code'
                                                    control={TextArea}
                                                    placeholder='Re-Upload Code'
                                                    onChange={this.handleChange}
                                                    value={this.state.reupload}
                                                />
                                                <Button color='teal' fluid size='large' onClick={this.handleReUpload}>Re-Upload</Button>
                                            </Form>
                                        ) : <div></div>
                                    } */}
                                </Segment>
                            </Grid.Column>
                        </Grid>
                        <br /><br />
                    </div>
                </div>
            </div>
        );
    }
}
