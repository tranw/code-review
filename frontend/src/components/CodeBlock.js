import React, { Component } from 'react';
import Highlight from 'react-highlight';
import { Grid, Icon, Form, TextArea, Button, Header, Segment, Modal } from 'semantic-ui-react';
import styles from './../../node_modules/highlight.js/styles/monokai.css';
import CommentModal from './CommentModal';
import ReactDOM from 'react-dom';

export default class CodeBlock extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modalOpen: false,
			currentComment: ''
		}
	}

	componentDidMount() {
		let key = 0;
		let lines = this.props.code.split('\n').map((line) => {
			key += 1;
			return (
				<Line value={line} key={key} line={key} />
			);
		});

		ReactDOM.render(
			<Highlight className={this.props.language} margi>
				{lines}
			</Highlight>,
			document.getElementById('code-mounting')
		);
	}
	
	handleOpen = (comment) => this.setState({ modalOpen: true, currentComment: comment })
	handleClose = () => this.setState({ modalOpen: false })
	render() {

		var commentItems = this.props.comments.map((comment) =>
			comment.hasComment ?
				<Icon key={comment.line_start}
					name='comments'
					size='big'
					style={{ marginLeft: '1em', cursor: 'pointer', color: '#ffc61e' }}
					onClick={() => this.handleOpen(comment.comment)}
				/>
				: <div style={{ height: '20px' }} key={comment.line_start} />
		);
		return (
			<div>
				<Modal open={this.state.modalOpen} onClose={this.handleClose}>
					<Modal.Header>Comment</Modal.Header>
					<Modal.Content image scrolling>
						<Modal.Description>
							{this.state.currentComment}
						</Modal.Description>
					</Modal.Content>
					<Modal.Actions>
						<Button primary onClick={this.handleClose}>
							Okay <Icon name='right chevron' />
						</Button>
					</Modal.Actions>
				</Modal>

				<Grid divided='vertically'>
					<Grid.Row columns={2}>
						<Grid.Column width={14} style={{ paddingLeft: 0, paddingRight: 0 }}>
							<div id='code-mounting'>

							</div>
						</Grid.Column>
						<Grid.Column width={1} style={{ marginTop: '-10px', marginLeft: '18px' }}>
							<h4>Comments</h4>
							{commentItems}
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

export class Line extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div id={this.props.line} className='code-line'>
				{this.props.value}
			</div>
		);
	}
}
