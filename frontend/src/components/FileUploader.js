import React from 'react';
import TagsInput from 'react-tagsinput';
import { Button, Input, Segment, TextArea, Dropdown } from 'semantic-ui-react';
import { getTokenHeader, instance } from '../utils/AuthService';
import { languageOptions } from '../constants/constants';

export default class FileUploader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            language: "",
            code: "",
            title: "",
            tags: []
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        const token = getTokenHeader();
        const data = {
            "language": this.state.language,
            "code": this.state.code,
            "title": this.state.title,
            "tags": this.state.tags
        };

        instance.post('/review/', data, {
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            }
        })
        .then((response) => {
            if (this.props.shouldRedirect){
                this.props.redirect.push('/code/' + response.data.id);
            }
        })
        .catch((data) => {
        });
    }

    render() {
        return (
            <Segment textAlign="center" stacked>
                <Input placeholder="Title of program"
                    id='form-input-title'
                    name='title'
                    value={this.state.title}
                    onChange={(e) => {this.setState({title:e.target.value})}}
                    style={{marginTop: '12px', marginBottom: '12px', width: '100%'}}
                />
                <TextArea
                    name='code'
                    placeholder='Paste your code here...'
                    value={this.state.code}
                    autoHeight={false}
                    onChange={(e) => {this.setState({code:e.target.value})}}
                    onClick={this.props.focusCode}
                    style={{marginTop: '12px', marginBottom: '12px'}}
                />
                <Dropdown
                    id='dropdown'
                    placeholder='Select language'
                    fluid selection
                    options={languageOptions}
                    onChange={(e, z) => {this.setState({language: z.value})}}
                    style={{marginTop: '12px', marginBottom: '12px'}}
                />
                <TagsInput 
                    value={this.state.tags} 
                    onChange={(tags) => {this.setState({tags})}}
                    style={{marginTop: '12px', marginBottom: '12px'}}
                />
                <Button 
                    color='teal' 
                    fluid ={true}
                    size='large' 
                    onClick={this.handleClick}
                    style={{marginTop: '12px', marginBottom: '12px'}}
                >Upload</Button>
            </Segment>
        );
    }
}
