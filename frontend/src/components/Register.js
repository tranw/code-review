import React from 'react';
import ReactDOM from 'react-dom';
import { loggedIn, login, instance } from '../utils/AuthService';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import '../style.css';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            username: '',
            password: '',
            passwordcheck: ''
        };
    }

    componentWillMount() {
        if (loggedIn()) {
            this.props.history.replace('/');
        }
    }

    handleRegistration = () => {
        if (this.state.password !== this.state.passwordcheck)
            return;

        instance.post('/accounts/register/', this.state)
            .then((e) => {
                login(this.state.username, this.state.password, this.props);
            })
            .catch((e) => {
                const errors = Object.keys(e.response.data).map((key) => {
                    return <Message negative={true} > {(key !== "non_field_errors") ?
                        key.toProperCase().replace("_", " ") : "Error"}: {e.response.data[key]} </Message>
                })

                ReactDOM.render(
                    <Segment>
                        {errors}
                    </Segment>,
                    document.getElementById('error-mount')
                );
            });
    };

    render() {
        return (
            <div className='login-form'>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' textAlign='center'>
                            {' '} Register
                            </Header>
                        <Form size='large'>
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    placeholder='First Name'
                                    value={this.state.first_name}
                                    onChange={(e) => { this.setState({ first_name: e.target.value }) }}
                                />
                                <Form.Input
                                    fluid
                                    placeholder='Last Name'
                                    value={this.state.last_name}
                                    onChange={(e) => { this.setState({ last_name: e.target.value }) }}
                                />
                                <Form.Input
                                    fluid
                                    placeholder='Username'
                                    value={this.state.username}
                                    onChange={(e) => { this.setState({ username: e.target.value }) }}
                                />
                                <Form.Input
                                    fluid
                                    placeholder='Password'
                                    type='password'
                                    value={this.state.password}
                                    onChange={(e) => { this.setState({ password: e.target.value }) }}
                                />
                                <Form.Input
                                    fluid
                                    placeholder='Confirm Password'
                                    type='password'
                                    value={this.state.passwordcheck}
                                    onChange={(e) => { this.setState({ passwordcheck: e.target.value }) }}
                                />
                                <Form.Input
                                    fluid
                                    placeholder='Email'
                                    type='email'
                                    value={this.state.email}
                                    onChange={(e) => { this.setState({ email: e.target.value }) }}
                                />
                                <Button id="reg-button" type="submit" value="SUBMIT" color='teal' fluid size='large' onClick={this.handleRegistration}>Register</Button>
                            </Segment>
                        </Form>
                        <div id='error-mount'>
                        </div>
                        <Message>
                            Meant to login? <a href='/login'>Here you go</a>.
                            </Message>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Register;