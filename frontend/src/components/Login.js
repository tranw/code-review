import React from 'react';
import ReactDOM from 'react-dom';
import { loggedIn, login } from '../utils/AuthService';
import { url } from '../constants/constants';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import '../style.css';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showMsg: false,
            errorMsgs: ''
        };

        this.handleLogin = this.handleLogin.bind(this);
    }

    componentWillMount() {
        if (loggedIn()) {
            this.props.history.replace('/');
        }

        console.log(url);
    }

    handleUsernameChange = e => {
        this.setState({ username: e.target.value });
    };

    handlePasswordChange = e => {
        this.setState({ password: e.target.value });
    };

    handleLogin() {
        this.setState({
            showMsg: false
        })

        login(this.state.username,
            this.state.password,
            this.props,
            (e) => {
                const messages = Object.keys(e.response.data).map((key) => {
                    return <Message negative={true} > {(key !== "non_field_errors") ? key.toProperCase() : "Error"}: {e.response.data[key]} </Message>
                });

                ReactDOM.render(
                    <Segment hidden={this.state.showMsg}>
                        {messages}
                    </Segment>,
                    document.getElementById('error-mount')
                );

                this.setState({
                    showMsg: true
                })
            }
        );
    };

    render() {
        return (
            <div className='login-form'>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' textAlign='center'>
                            {' '} Code Review Login
                            </Header>
                        <Form size='large'>
                            <Segment stacked>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='Username'
                                    value={this.state.username}
                                    onChange={this.handleUsernameChange}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    value={this.state.password}
                                    onChange={this.handlePasswordChange}
                                />
                                <Button id="login-button" type="submit" value="SUBMIT" color='teal' fluid size='large' onClick={this.handleLogin}>Login</Button>
                            </Segment>
                            <div id='error-mount'>
                            </div>
                        </Form>
                        <Message>
                            New to us? <a href='/register'>Sign Up</a>
                        </Message>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Login;