import React from 'react';
import FileUploader from './FileUploader';
import FileCard from './FileSelector';
import { getUserFiles } from '../utils/APIService';
import { Header, Grid, Divider } from 'semantic-ui-react';
import 'react-tagsinput/react-tagsinput.css'

export default class Home extends React.Component {
	constructor(props) {
		super(props);

		this.state = { files: [] }
	}

	componentWillMount() {
		getUserFiles().then((z) => {
			let data = z.data.map((file) => {
				return (
					<Grid.Column key={file.id}>
						<FileCard id={file.id} title={file.title} language={file.language} commentCount={file.comments.length} />
					</Grid.Column>
				);
			})

			this.setState({ files: data })
		})
	}


	render() {
		return (
			<div>
				<div
					id='file-upload'
					style={{ zIndex: '999' }}
					onClick={() => { this.setState({ uploading: true }) }}
				>
					<Header as='h1' textAlign={'center'}>
						Upload a File
					</Header>
					<FileUploader
						redirect={this.props.history}
						shouldRedirect={true}
					/>
				</div>
				<Divider
					horizontal
					style={{ marginTop: '64px', marginBottom: '64px' }}
				>Uploads</Divider>
				<div>
					<Grid columns='6' padded='horizontally' stackable >
						{this.state.files}
					</Grid>
				</div>
			</div>
		);
	}
}
