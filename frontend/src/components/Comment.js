import React from 'react';
import ReactDOM from 'react-dom';
import React, { Component } from 'react';

export default class Comment extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: ""
        };
    }

    render() {
        return (
            <div>
                <div id='title-code'>
                    <Header as='h1'>
                        {this.state.title}
                    </Header>
                </div>
                <div id='code-root'>
                </div>
            </div>
        );
    }
}

var data = [
    {id: 1, author: "Reviewer", text: "Comment"},
];

var CommentList = React.createClass({
    render: function() {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Comment author={comment.author} key={comment.id}>
                    {comment.text}
                </Comment>
            );
        });
        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});
var CommentBox = React.createClass({
    render: function() {
        return (
            <div className="commentBox">
                <h1>Comments</h1>
                <CommentList data={this.props.data} />
                <CommentForm />
            </div>
        );
    }
});
ReactDOM.render(
    <CommentBox data={data} />,
    document.getElementById('content')
);


