import React from 'react';
import { Card, Image, Button, Label } from 'semantic-ui-react'
import { getPicture } from '../constants/constants';
import { Redirect } from 'react-router-dom'

const titleCutoff = 20;

export default class FileCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false,
        }
    }

    setRedirect = () => {
        this.setState({ redirect: true });
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={'/code/' + this.props.id} />
        }
    };

    render() {
        let title = this.props.title.substring(0, titleCutoff);
        let pictureLink = getPicture(this.props.language);

        return (
            <Card>
                <Card.Content>
                    <Card.Header>
                        {(title.length > titleCutoff) ? title + "..." : title}
                        <Image avatar floated='right' size='small' src={pictureLink} />
                    </Card.Header>
                    <Card.Meta>
                        {this.props.language}
                    </Card.Meta>
                </Card.Content>
                <Card.Content extra>
                    <Button fluid={true} floated='left' color={'teal'} onClick={this.setRedirect}>
                        View
                    </Button>
                    <Label floated='right' style={{ width: '100%' }}>
                        <div>
                            Comments
                        <Label.Detail style={{ float: 'right' }}>
                                {this.props.commentCount}
                            </Label.Detail>
                        </div>
                    </Label>
                </Card.Content>
                {this.renderRedirect()}
            </Card>
        )
    }
}
