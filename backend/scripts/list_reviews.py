import requests
import get_token
import sys

def main():
	if len(sys.argv) != 3:
		print("Please supply username and password")
		sys.exit(127)

	token = get_token.get_token(sys.argv[1], sys.argv[2])
	h = {'Authorization' : 'Token {}'.format(token.split(':')[1].split('"')[1])}
	print(h)
	r = requests.get('http://localhost:8000/review/3', headers=h)
	print(r.text)

if __name__ == '__main__':
	main()