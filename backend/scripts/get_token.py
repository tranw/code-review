import requests
import sys

def main():
	if len(sys.argv) != 3:
		print("Please enter username password")
		sys.exit(127)

	username = sys.argv[1]
	pw = sys.argv[2]

	r = requests.post('http://localhost:8000/auth', data={'username':username, 'password':pw})
	print(r.text)

def get_token(username, password):
	r = requests.post('http://localhost:8000/auth', data={'username':username, 'password':password})
	return r.text

if __name__ == '__main__':
	main()