from django.conf import settings
from django.conf.urls import url, include
from django.views.generic import TemplateView

# Django 2.0.x include tuple fix
django2 = include.__code__.co_argcount == 2

def build_url(regex, arg, namespace):
    if django2:
        return url(regex, include((arg, namespace), namespace=namespace))
    else:
        return url(regex, include(arg, namespace=namespace, app_name=namespace))

api_patterns = [
    build_url(r'accounts/', 'accounts.urls', 'accounts'),
    build_url(r'review/', 'review.urls', 'review'),
]

#Keep these in the order they're in,
#if api isn't first then it'll match the other
#url for react, instead of api.
urlpatterns = [
	build_url(r'^api/', api_patterns, 'api'), #Dispatches to API
]

if not settings.DEBUG:
    urlpatterns += [
        url(r'.*', TemplateView.as_view(template_name='index.html'), name='react-index') #Dispatches to React
    ]

