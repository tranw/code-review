from backend.devsettings import *

DEBUG = False

ALLOWED_HOSTS = ['code-feedback.proj.cs.wwu.edu']

CORS_ORIGIN_WHITELIST = (
    'code-feedback.proj.cs.wwu.edu'
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': '/usr/local/etc/mysql/code-review.cnf',
        }
    }
}

STATICFILES_DIR = [
    os.path.join(os.path.dirname(BASE_DIR), 'frontend/build/static'),
    os.path.join(os.path.dirname(BASE_DIR), 'frontend/build/assets')
]