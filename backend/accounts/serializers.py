from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework.validators import UniqueValidator

UserModel = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=UserModel.objects.all())]
    )

    username = serializers.CharField(
        validators=[UniqueValidator(queryset=UserModel.objects.all())]
    )

    first_name = serializers.CharField(
        required=True,
    )
    
    last_name = serializers.CharField(
        required=True,
    )

    is_superuser = serializers.BooleanField(required=False)
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = UserModel.objects.create_user(last_name=validated_data['last_name'],
            first_name=validated_data['first_name'],
            username=validated_data['username'], 
        	email=validated_data['email'],
            password=validated_data['password'])
        return user

    class Meta:
        model = UserModel
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'password', 'is_superuser')