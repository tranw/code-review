# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions, status
from django.contrib.auth import get_user_model
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import TokenAuthentication
from django.contrib.auth.models import Group

from .serializers import UserSerializer, UserModel

class UserCreate(APIView):
	"""
	User registration
	"""

	permission_classes = (permissions.AllowAny,)

	def post(self, request, format='json'):
		serializer = UserSerializer(data=request.data)

		if serializer.is_valid():
			user = serializer.save()

			student_group, created = Group.objects.get_or_create(name='student')
			student_group.user_set.add(user)

			if user:
				return Response(serializer.data, status=status.HTTP_201_CREATED)

		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserView(APIView):
	permission_classes = (permissions.IsAuthenticated, )

	def get(self, request, format='json'):
		return Response(UserSerializer(self.request.user).data, status=status.HTTP_200_OK)