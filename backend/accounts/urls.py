from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views as local_views
from rest_framework.authtoken import views as rest_framework_views

urlpatterns = [
    url(r'register/$', local_views.UserCreate.as_view(), name='register'),
    url(r'get_user_info/$', local_views.UserView.as_view(), name='user_info'),
    url(r'get_auth_token/$', rest_framework_views.obtain_auth_token, name='get_auth_token'),
    #url(r'^request_reviewer/$', local_views.request_reviewer, name='request_reviewer')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
