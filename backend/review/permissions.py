from rest_framework.permissions import BasePermission


class UserIsReviewOwner(BasePermission):

    def has_object_permission(self, request, view, review):
        return request.user.id == review.user.id
