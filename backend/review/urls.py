from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views as local_views

urlpatterns = [
    url(r'^$', local_views.ReviewListCreateAPIView.as_view(), name='review_list'),
    url(r'^(?P<pk>[0-9]+)/$', local_views.ReviewDetailAPIView.as_view(), name='review_detail'),
    url(r'^comment/$', local_views.CommentListCreateAPIView.as_view(), name='comment_list'),
    url(r'^comment/(?P<pk>[0-9]+)/$', local_views.CommentDetailAPIView.as_view(), name='comment_detail'),
    url(r'^search/(?P<term>[\w|\W]+)/$', local_views.SearchView.as_view(), name='search_list')
    
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
