# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth.models import User #TODO: Change to get_user_model

from review.models import Review, Tag, Comment
from review.permissions import UserIsReviewOwner
from review.serializers import ReviewSerializer, CommentSerializer, TagSerializer
from accounts.serializers import UserSerializer


class ReviewListCreateAPIView(ListCreateAPIView):
	serializer_class = ReviewSerializer
	#permission_classes = (IsAuthenticated, )
	permission_classes = (AllowAny, )

	def get_queryset(self):
		return Review.objects.filter(user=self.request.user)

	def perform_create(self, serializer):
		file = serializer.save(user=self.request.user)
		for tag in self.request.data['tags']:
			new_tag = Tag.objects.create(name=tag.lower(), file=file)

class ReviewDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = ReviewSerializer
	queryset = Review.objects.all()
	#permission_classes = (IsAuthenticated, UserIsReviewOwner)
	permission_classes = (AllowAny, )

class CommentListCreateAPIView(ListCreateAPIView):
	serializer_class = CommentSerializer

	#Add file ownership permission
	permission_classes = (IsAuthenticated, )

	def get_queryset(self):
		return Comment.objects.filter(user=self.request.user)

	def perform_create(self, serializer):
		review = Review.objects.get(id=self.request.data['id'])

		serializer.save(
			reviewer=self.request.user,
			file=review
		)

class CommentDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = CommentSerializer
	queryset = Comment.objects.all()
	permission_classes = (AllowAny, )

class TagsListView(ListCreateAPIView):
	#would add verification if reviewer here
	permission_classes = (AllowAny, )
	serializer_class = TagSerializer

	def list(self, request, search):
		queryset = Tag.objects.filter(name=search)
		serializer = TagSerializer(queryset, many=True)
		return Response(serializer.data)

	def get_queryset(self):
		return Tag.objects.all()

class SearchView(APIView):
	#authentication_classes = (IsAuthenticated, )
	#permission_classes = (,) #Is reviewer

	def get(self, request, term, format=None):
		#Grab the three queries seperately, doing so to delegate work
		#to the database, instead of us having to pattern match.
		languages	= list(Review.objects.filter(language__contains=term))
		titles  	= list(Review.objects.filter(title__contains=term))
		tags 		= [tag.file for tag in Tag.objects.filter(name__contains=term)]
		
		#Serialize a combined list to send to the frontend
		serializer = ReviewSerializer(titles + tags + languages, many=True)

		#Send the response containing the data.
		return Response(serializer.data)




