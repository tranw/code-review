from django.conf import settings
from django.db import models

class Review(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField('Title', max_length=50)
    language = models.CharField('Language', max_length=20)
    code = models.CharField('Code', max_length=20000)

class Tag(models.Model):
    name = models.CharField(max_length=30)
    file = models.ForeignKey(Review, on_delete=models.CASCADE, related_name='file', null=True)

class Comment(models.Model):
	reviewer = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='reviewer', on_delete=models.CASCADE)
	file = models.ForeignKey(Review, on_delete=models.CASCADE, related_name='comments')
	text = models.CharField('Comment', max_length=500)
	line_start = models.IntegerField(default=0)
	line_end = models.IntegerField(default=0)