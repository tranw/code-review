# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-12 07:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0006_auto_20180311_2349'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='tags',
        ),
        migrations.AddField(
            model_name='tag',
            name='file',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tags', to='review.Review'),
        ),
    ]
