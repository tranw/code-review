import subprocess
import os
import sys
import threading

def main():
    python_cmd = sys.executable
    r = threading.Thread(target=begin_react, args=(python_cmd,))
    d = threading.Thread(target=begin_django, args=(python_cmd,))
    d.start()
    r.start()

def begin_django(python_cmd):
    subprocess.call("{} ./backend/manage.py runserver --settings=backend.devsettings".format(python_cmd), shell=True)

def begin_react(python):
    os.chdir("./frontend/")
    subprocess.call("npm update", shell=True)
    subprocess.call("npm start", shell=True)

if __name__ == '__main__':
	main()
